package com.mungmang.book.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 업로드 테스트
 * */
@Controller
public class ImageController {

    @GetMapping(value = "/index")
    public String readIndex() {
        return "index";
    }
}
