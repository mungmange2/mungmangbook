package com.mungmang.book.springboot.controller.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor //final 필드가 포함된 생성자를 생성. final 으로 선언되지 않는 객체들은 제외
public class HelloResponseDto {

    private final String name;
    private final int amount;

}