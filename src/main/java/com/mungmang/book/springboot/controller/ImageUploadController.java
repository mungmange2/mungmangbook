package com.mungmang.book.springboot.controller;
import com.mungmang.book.springboot.controller.dto.UploadDto.UploadRequest;
import com.mungmang.book.springboot.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * 업로드 테스트
 * */
@RestController
@RequestMapping(value = "/images")
@RequiredArgsConstructor
public class ImageUploadController {

    private final FileService fileService;

    @PostMapping(value = "/upload")
    public ResponseEntity createImages(@ModelAttribute UploadRequest uploadRequest) throws Exception {
        this.fileService.uploadFiles(uploadRequest.getImages());
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
