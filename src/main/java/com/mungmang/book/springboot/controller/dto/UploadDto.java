package com.mungmang.book.springboot.controller.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

public class UploadDto {

    @Data
    public static class UploadRequest {
        private String name;
        private MultipartFile[] images;
    }
}
