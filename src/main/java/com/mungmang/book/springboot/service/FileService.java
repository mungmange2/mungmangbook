package com.mungmang.book.springboot.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Slf4j
@Service
public class FileService {
    public void uploadFiles(MultipartFile[] files) throws Exception {
        String uploadPath = new File("").getAbsolutePath() + "/web/image/";
        for (MultipartFile file : files) {
            try {
                if (!ObjectUtils.isEmpty(file.getOriginalFilename())) {
                    log.info("fileName:{}", file.getOriginalFilename());
                    File target = new File(uploadPath, file.getOriginalFilename());
                    file.transferTo(target);
                }
            } catch (IOException e) {
                throw new Exception(e.getMessage());
            } catch (Exception e) {
                throw new Exception(e.getMessage());
            }
        } // end of for
    }
}
