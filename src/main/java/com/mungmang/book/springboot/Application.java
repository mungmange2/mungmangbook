package com.mungmang.book.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// 스프링 부트의 자동설정, 스프링 Bean 읽기와 생성을 모두 자동적으로 설정
// @SrpingBootApplication 위치부터 설정을 읽어가기 때문에 항상 프로젝트의 최상단에 위치해야 함.
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        // SpringApplication.run 으로 인해 내장 WAS 실행 -> 언제 어디서나 스프링부트를 배포 가능.
        SpringApplication.run(Application.class, args);
    }
}
