package com.mungmang.book.springboot.controller;

import com.mungmang.book.springboot.service.FileService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ImageUploadController.class)
public class ImageUploadControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FileService fileService;

    @Test
    public void fileUploadTest() throws Exception{

        String uploadPath = new File("").getAbsolutePath() + "/web/test-image/";

        FileInputStream fis = new FileInputStream(uploadPath + "스크린샷 2021-06-07 오후 11.27.57.png");
        MockMultipartFile multipartFile = new MockMultipartFile("images", fis);

        HashMap<String, String> contentTypeParams = new HashMap<>();
        contentTypeParams.put("name", "aaaaa");
        MediaType mediaType = new MediaType("multipart", "form-data", contentTypeParams);

        this.mockMvc.perform(multipart("/images/upload")
                .content(multipartFile.getBytes())
                .contentType(mediaType))
                .andExpect(status().isCreated());
    }
}
